from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Post

# Function View
def home(request):
    context = {
        'posts': Post.objects.all()
    }
    print(context)
    return render(request, 'blog/home.html', context)

# Class Base View
class PostListView(ListView):
    model = Post
    # Default <app>/<model>_<viewtype>.html Ex: blog/post_list.html
    # Override to blog/home.html
    template_name = 'blog/home.html'
    # Default listView variable will call object_list instead of posts
    # Override object variable list variable
    context_object_name = 'posts'
    # Sort Latest to the top
    ordering = ['-date_posted']


class PostDetailView(DetailView):
    model = Post
    # default template => blog/post_detail.html
    # default variable => object
    # default order => asc


# LoginRequiredMixin Add require login functionaility to the view
class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    # Override
    def form_valid(self, form):
        # Before submit validate form set form instance author to request user
        form.instance.author = self.request.user
        # Invoke parent form_valid()
        return super().form_valid(form)

# UserPassesTestMixin 
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    # will run if the user pass the certain test condition
    def test_func(self):
        # get post object
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    # Successful delete redirect to home Page
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})