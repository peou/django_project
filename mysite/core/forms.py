from django import forms
from core.models import Rating, Restaurant
from django.core.validators import MinValueValidator, MaxValueValidator

# class RatingForm(forms.ModelForm):
class RatingForm(forms.Form):
    # class Meta:
    #     model = Rating
    #     fields = ("restaurant", "user", "rating")

    rating = forms.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])

class RestaurantForm(forms.ModelForm):
    class Meta:
        model = Restaurant
        fields = ("name","restaurant_type")