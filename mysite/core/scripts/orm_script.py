from django.contrib.auth.models import User
from core.models import Restaurant, Rating, Sale, Staff, StaffRestaurant
from django.utils import timezone
from django.db import connection
from pprint import pprint
from django.db.models.functions import Lower
from django.db.models import Sum, Prefetch

def run():
    # restaurant = Restaurant.objects.filter(name="Pizzeria 1")
    # restaurant = Restaurant.objects.get(name="Pizzeria 1")
    # print(restaurant) # return query set
    # print(restaurant.get()) # return single row

    # chinese = Restaurant.TypeChoices.CHINESE

    
    chinese = Restaurant.TypeChoices.CHINESE
    indian = Restaurant.TypeChoices.INDIAN
    mexican = Restaurant.TypeChoices.MEXICAN
    check_types = [chinese, indian, mexican]
    
    # restaurants = Restaurant.objects.filter(restaurant_type=chinese, name__startswith='c')
    # restaurants = Restaurant.objects.filter(restaurant_type__in=check_types)
    # restaurants = Restaurant.objects.exclude(restaurant_type=chinese)
    # restaurants = Restaurant.objects.exclude(restaurant_type__in=check_types)
    # restaurants = Restaurant.objects.filter(name__lt='E')
    # restaurants = Restaurant.objects.filter(longitude__gt=0)
    # restaurants = Restaurant.objects.filter(longitude__gte=0)
    # restaurants = Restaurant.objects.filter(longitude__lte=0)
    # restaurants = Restaurant.objects.order_by('name').reverse() # = order_by('-name')
    
    # r = Restaurant.objects.first()
    # r.name = r.name.lower()
    # r.save() 

    # restaurants = Restaurant.objects.order_by('name')
    # restaurants = Restaurant.objects.order_by(Lower('name'))
    # restaurants = Restaurant.objects.order_by('date_opened')[:5]
    # restaurants = Restaurant.objects.order_by('date_opened')[2:5]
    
    # restaurants = Restaurant.objects.all()

    # restaurants = Restaurant.objects.latest()
    # print(restaurants)
    
    # sales = Sale.objects.filter(income__range=(50,60))
    # print( [ sale.income for sale in sales ] )

    # ratings = Rating.objects.filter(restaurant__name__startswith='G') 
    # print(ratings)
    # sales = Sale.objects.filter(restaurant__restaurant_type=chinese)
    # print(sales)
    
    # staff, created = Staff.objects.get_or_create(name='John Wick')

    # staff.restaurant.add(Restaurant.objects.first())
    # print(staff.restaurant.all())
    # staff.restaurant.remove(Restaurant.objects.first())
    # staff.restaurant.set(Restaurant.objects.all()[:10]) # add bulk
    # staff.restaurant.clear() # remove all associate
    # print(staff.restaurant.count())
    
    # italian = staff.restaurant.filter(restaurant_type=Restaurant.TypeChoices.INDIAN)
    # print(italian)

    # restaurant = Restaurant.objects.get(pk=6)
    # print(restaurant.staff_set.all())

    # staff, created = Staff.objects.get_or_create(name='John Wick')
    # restaurant = Restaurant.objects.first()
    # restaurant2 = Restaurant.objects.last()

    # StaffRestaurant.objects.create(
    #     staff=staff, restaurant = restaurant, salary=28_000
    # )
    # StaffRestaurant.objects.create(
    #     staff=staff, restaurant = restaurant2, salary=24_000
    # )

    # restaurant = Restaurant.objects.prefetch_related('ratings', 'sales') # 1 to 1, 1 to many
    # restaurant = Restaurant.objects.filter(name__istartswith='c').prefetch_related('ratings', 'sales')
    # print(restaurant)

    # ratings = Rating.objects.select_related("restaurant")
    # ratings = Rating.objects.only('rating', 'restaurant__name').select_related("restaurant")
    # print(ratings)

    # restaurant = Restaurant.objects.prefetch_related('ratings', 'sales')\
    #                 .filter(ratings__rating=5) \
    #                 .annotate(total=Sum('sales__income'))
    # print(restaurant)

    month_ago = timezone.now() - timezone.timedelta(days=30)
    monthly_sales = Prefetch(
        'sales',
        queryset=Sale.objects.filter(datetime__gte=month_ago)
    )
    restaurant = Restaurant.objects.prefetch_related('ratings', monthly_sales).filter(ratings__rating=5)
    restaurant = restaurant.annotate(total=Sum('sales__income'))
    print([r.total for r in restaurant])
    # print(restaurant)

    pprint(connection.queries)

    
    
    




